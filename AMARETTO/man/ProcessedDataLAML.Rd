\name{ProcessedDataLAML}
\alias{ProcessedDataLAML}
\docType{data}
\title{
	Downloaded date for LAML: the TCGA AML multi-omics data. 
}
\description{
	Example data including preprocessed gene expression data, GISTIC copy number 
	data and MethylMix genes for the TCGA LAML project. LAML is the TCGA code 
	for the acute myeloid luekemia project. This data was downloaded using the 
	download and preprocess scripts in this package. 
}
\usage{ProcessedDataLAML}

\source{
    https://tcga-data.nci.nih.gov/tcga/
    }
\value{
    No value returned, as this is a dataset.
}
\references{
    Cancer Genome Atlas Research Network. Genomic and epigenomic landscapes of
	adult de novo acute myeloid leukemia. N Engl J Med. 2013 May 
	30;368(22):2059-74. doi: 10.1056/NEJMoa1301689. Epub 2013 May 1. Erratum in: 
	N Engl J Med. 2013 Jul 4;369(1):98. PubMed PMID: 23634996; PubMed Central 
	PMCID: PMC3767041.
}
%\examples{
%data(ProcessedDataLAML)
%}
\keyword{datasets}